﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using SmartCOM3Lib;
using ru.sazan.trader.Data;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.smartcom.Models;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.smartcom
{
    public class SmartComOrderManager:OrderManager
    {
        private GenericSingleton<StServer> singleton;
        private BaseDataContext rawData;
        private Logger logger;

        public SmartComOrderManager() :
            this(new StServerSingleton(), RawTradingData.Instance, DefaultLogger.Instance) { }

        public SmartComOrderManager(GenericSingleton<StServer> singleton, BaseDataContext rawData, Logger logger)
        {
            this.singleton = singleton;
            this.rawData = rawData;
            this.logger = logger;
        }

        public void PlaceOrder(Order order)
        {
            RawOrderFactory factory = new RawOrderFactory(new FortsTradingSchedule());

            RawOrder rawOrder = factory.Make(order);

            this.singleton.Instance.PlaceOrder(rawOrder.Portfolio, rawOrder.Symbol, rawOrder.Action, rawOrder.Type, rawOrder.Validity, rawOrder.Price, rawOrder.Amount, rawOrder.Stop, rawOrder.Cookie);

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, отправлена заявка {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}", 
                BrokerDateTime.Make(DateTime.Now), 
                this.GetType().Name,
                rawOrder.Cookie, 
                rawOrder.Portfolio, 
                rawOrder.Symbol, 
                rawOrder.Action, 
                rawOrder.Type, 
                rawOrder.Validity, 
                rawOrder.Price,
                rawOrder.Amount, 
                rawOrder.Stop));
        }

        public void MoveOrder(Order order, double price)
        {
        }

        public void CancelOrder(Order order)
        {
            UpdateOrder update = GetUpdate(order.Id);

            if (update == null)
                return;

            this.singleton.Instance.CancelOrder(order.Portfolio, order.Symbol, update.OrderId);

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, отправлен запрос на отмену заявки {2}", 
                BrokerDateTime.Make(DateTime.Now),
                this.GetType().Name, 
                order.ToString()));
        }

        private IEnumerable<UpdateOrder> GetUpdates(int orderId)
        {
            try
            {
                return this.rawData.GetData<UpdateOrder>().Where(u => u.Cookie == orderId);
            }
            catch
            {
                return null;
            }
        }

        private UpdateOrder GetUpdate(int orderId)
        {
            try
            {
                return GetUpdates(orderId).Last();
            }
            catch
            {
                return null;
            }
        }
    }
}
