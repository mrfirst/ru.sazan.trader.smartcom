﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.smartcom.Events;
using ru.sazan.trader.smartcom.Data;
using SmartCOM3Lib;
using ru.sazan.trader.Net;
using ru.sazan.trader.smartcom.Net;
using ru.sazan.trader.Configuration;
using ru.sazan.trader.Events;
using ru.sazan.trader.Data;
using System.Runtime.InteropServices;
using System.Threading;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.smartcom
{
    public class SmartComAdapter : Subject, Service
    {
        private GenericSingleton<StServer> stServerSingleton;
        private StServer stServer;
        private SmartComHandlersDatabase handlers;
        private Binder binder;
        private Subscriber subscriber;
        private Logger logger;
        private Connector connector;
        private DataContext defaultAdapterHandlers;
        private Timer dataFlowMonitorTimer, connectionMonitorTimer;
        private int seconds, monitorTimeoutSeconds;

        public SmartComAdapter()
            : this(new SmartComConnector(), 
            SmartComHandlers.Instance, 
            DefaultBinder.Instance, 
            DefaultSubscriber.Instance, 
            new StServerSingleton(), 
            DefaultLogger.Instance) { }

        public SmartComAdapter(Connector connector, 
            SmartComHandlersDatabase handlers, 
            Binder binder, 
            Subscriber subscriber, 
            GenericSingleton<StServer> stServerSingleton, 
            Logger logger,
            int monitorTimeoutSeconds = 20)
        {
            this.stServerSingleton = stServerSingleton;
            this.stServer = this.stServerSingleton.Instance;
            this.handlers = handlers;
            this.binder = binder;
            this.subscriber = subscriber;
            this.logger = logger;
            this.connector = connector;
            this.monitorTimeoutSeconds = monitorTimeoutSeconds;
            this.defaultAdapterHandlers = new AdapterHandlers();
            this.seconds = AppSettings.GetValue<int>("SecondsBetweenConnectionAwaitingAttempts");

            this.handlers.Add<_IStClient_ConnectedEventHandler>(SmartComTraderConnected);
            this.handlers.Add<_IStClient_DisconnectedEventHandler>(SmartComTraderDisconnected);

            this.dataFlowMonitorTimer = new Timer(MonitorDataFlow, null, this.monitorTimeoutSeconds * 1000, 1000 * this.seconds);
            this.connectionMonitorTimer = new Timer(MonitorConnection, null, this.monitorTimeoutSeconds * 1000, 1000 * this.seconds);
        }

        private void MonitorConnection(object state)
        {
            if (this.isRunning && !this.connector.IsConnected)
                this.connector.Connect();
        }

        private void MonitorDataFlow(object state)
        {
            if (this.isRunning && this.connector.IsConnected)
                this.stServer.GetPrortfolioList();
        }

        public void Start()
        {
            this.isRunning = true;

            BindHandlers();

            this.connector.Connect();
        }

        private void BindHandlers()
        {
            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, выполняется связывание обработчиков SmartCom", 
                BrokerDateTime.Make(DateTime.Now), 
                this.GetType().Name));
            this.binder.Bind();
        }


        public void Stop()
        {
            this.isRunning = false;

            Unsubscribe();

            this.connector.Disconnect();

            UnbindHandlers();
        }

        private void Reconnect()
        {
            this.connector.Connect();
        }

        private void Unsubscribe()
        {
            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, отменяем подписки на получение данных", 
                BrokerDateTime.Make(DateTime.Now), 
                this.GetType().Name));
            this.subscriber.Unsubscribe();
        }

        private void UnbindHandlers()
        {
            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, выполняется отключение обработчиков SmartCom", 
                BrokerDateTime.Make(DateTime.Now), 
                this.GetType().Name));
            this.binder.Unbind();
        }

        public void Restart()
        {
            this.Stop();

            this.Start();
        }

        private bool isRunning;
        public bool IsRunning
        {
            get { return this.isRunning; }
        }

        private void SmartComTraderConnected()
        {
            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, соединение установлено", 
                BrokerDateTime.Make(DateTime.Now), 
                this.GetType().Name));
            this.subscriber.Subscribe();
        }

        private void SmartComTraderDisconnected(string reason)
        {
            if (reason.ToLower().Contains("disconnected by user"))
                return;

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, соединение неожиданно разорвано {2}", 
                BrokerDateTime.Make(DateTime.Now), 
                this.GetType().Name, 
                reason));
        }
    }
}
