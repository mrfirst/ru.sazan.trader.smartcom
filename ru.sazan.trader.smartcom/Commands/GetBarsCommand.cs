﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using SmartCOM3Lib;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.smartcom.Commands
{
    public class GetBarsCommand:Transaction
    {
        private string symbol;
        private int barIntervalSeconds;
        private int barQuantity;
        private GenericSingleton<StServer> stServerSingleton;
        private Logger logger;

        public GetBarsCommand(string symbol, int barIntervalSeconds, int barQuantity)
            : this(symbol, barIntervalSeconds, barQuantity, new StServerSingleton(), DefaultLogger.Instance) { }

        public GetBarsCommand(string symbol, int barIntervalSeconds, int barQuantity, GenericSingleton<StServer> stServerSingleton, Logger logger)
        {
            this.symbol = symbol;
            this.barIntervalSeconds = barIntervalSeconds;
            this.barQuantity = barQuantity;
            this.stServerSingleton = stServerSingleton;
            this.logger = logger;
        }

        public void Execute()
        {
            this.stServerSingleton.Instance.GetBars(this.symbol, StBarIntervalFactory.Make(this.barIntervalSeconds), BrokerDateTime.Make(DateTime.Now), this.barQuantity);
            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, отправлен запрос на получение Bar-ов {2}, {3}, {4}",
                BrokerDateTime.Make(DateTime.Now), 
                this.GetType().Name, 
                this.symbol, 
                this.barIntervalSeconds, 
                this.barQuantity));
        }
    }
}
