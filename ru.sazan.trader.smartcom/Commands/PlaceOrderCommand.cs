﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using SmartCOM3Lib;
using ru.sazan.trader.Models;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.smartcom.Models;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.smartcom.Commands
{
    public class PlaceOrderCommand
    {
        private Order order;
        private GenericSingleton<StServer> singleton;
        private Logger logger;

        public PlaceOrderCommand(Order order, GenericSingleton<StServer> singleton, Logger logger)
        {
            this.order = order;
            this.singleton = singleton;
            this.logger = logger;
        }

        public void Execute()
        {
            RawOrderFactory factory = new RawOrderFactory(new FortsTradingSchedule());

            RawOrder rawOrder = factory.Make(order);

            this.singleton.Instance.PlaceOrder(rawOrder.Portfolio, rawOrder.Symbol, rawOrder.Action, rawOrder.Type, rawOrder.Validity, rawOrder.Price, rawOrder.Amount, rawOrder.Stop, rawOrder.Cookie);

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, отправлена заявка {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}", 
                BrokerDateTime.Make(DateTime.Now), 
                this.GetType().Name,
                rawOrder.Cookie, 
                rawOrder.Portfolio, 
                rawOrder.Symbol, 
                rawOrder.Action, 
                rawOrder.Type, 
                rawOrder.Validity, 
                rawOrder.Price,
                rawOrder.Amount, 
                rawOrder.Stop));
        }
        
    }
}
