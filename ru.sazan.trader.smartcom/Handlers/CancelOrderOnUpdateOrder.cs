﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.smartcom.Models;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Configuration;
using ru.sazan.trader.Data;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.Models;
using SmartCOM3Lib;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.smartcom.Handlers
{
    public class CancelOrderOnUpdateOrder:GenericCollectionObserver<UpdateOrder>
    {
        private DataContext tradingData;
        private Logger logger;

        public CancelOrderOnUpdateOrder()
            : this(TradingData.Instance, RawTradingData.Instance, DefaultLogger.Instance) { }

        public CancelOrderOnUpdateOrder(DataContext tradingData, RawTradingDataContext rawData, Logger logger)
            :base(rawData)
        {
            this.tradingData = tradingData;
            this.logger = logger;
        }

        public override void Update(UpdateOrder item)
        {
            if (!IsCancelUpdate(item))
                return;

            Order order = FindOrder(item);

            if (order == null)
                return;

            order.Cancel(item.Datetime, item.State.ToString());

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, заявка отменена {2}", 
                BrokerDateTime.Make(DateTime.Now), 
                this.GetType().Name, 
                order.ToString()));
        }

        private Order FindOrder(UpdateOrder update)
        {
            try
            {
                return this.tradingData.Get<IEnumerable<Order>>().SingleOrDefault(o => o.Id == update.Cookie
                    && !o.IsFilled
                    && !o.IsRejected
                    && !o.IsExpired
                    && !o.IsCanceled);
            }
            catch
            {
                return null;
            }
        }

        private bool IsCancelUpdate(UpdateOrder update)
        {
            return update.State == StOrder_State.StOrder_State_Cancel ||
                update.State == StOrder_State.StOrder_State_SystemCancel ||
                update.State == StOrder_State.StOrder_State_ContragentCancel;
        }
    }
}
