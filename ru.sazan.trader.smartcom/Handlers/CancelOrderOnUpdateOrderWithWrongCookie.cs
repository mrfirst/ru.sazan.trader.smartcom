﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.smartcom.Models;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Configuration;
using ru.sazan.trader.Data;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.Models;
using SmartCOM3Lib;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.smartcom.Handlers
{
    public class CancelOrderOnUpdateOrderWithWrongCookie:GenericCollectionObserver<UpdateOrder>
    {
        private DataContext tradingData;
        private Logger logger;

        public CancelOrderOnUpdateOrderWithWrongCookie()
            : this(TradingData.Instance, RawTradingData.Instance, DefaultLogger.Instance) { }

        public CancelOrderOnUpdateOrderWithWrongCookie(DataContext tradingData, RawTradingDataContext rawData, Logger logger)
            :base(rawData)
        {
            this.tradingData = tradingData;
            this.logger = logger;
        }

        public override void Update(UpdateOrder item)
        {
            if (!IsCancelUpdate(item))
                return;

            CookieToOrderNoAssociation association = FindCookieToOrderNoAssociation(item.OrderNo);

            if (association == null)
                association = FindCookieToOrderNoAssociation(item.OrderId);

            if (association == null)
                return;

            Order order = FindOrder(association.Cookie);

            if (order == null)
                return;

            order.Cancel(item.Datetime, item.State.ToString());

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, заявка отменена {2}", 
                BrokerDateTime.Make(DateTime.Now), 
                this.GetType().Name, 
                order.ToString()));
        }

        private CookieToOrderNoAssociation FindCookieToOrderNoAssociation(string orderNo)
        {
            try
            {
                return this.dataContext.GetData<CookieToOrderNoAssociation>().SingleOrDefault(a => a.OrderNo == orderNo);
            }
            catch
            {
                return null;
            }
        }

        private Order FindOrder(int id)
        {
            try
            {
                return this.tradingData.Get<IEnumerable<Order>>().SingleOrDefault(o => o.Id == id
                    && !o.IsFilled
                    && !o.IsRejected
                    && !o.IsExpired
                    && !o.IsCanceled);
            }
            catch
            {
                return null;
            }
        }

        private bool IsCancelUpdate(UpdateOrder update)
        {
            return update.State == StOrder_State.StOrder_State_Cancel ||
                update.State == StOrder_State.StOrder_State_SystemCancel ||
                update.State == StOrder_State.StOrder_State_ContragentCancel;
        }
    }
}
