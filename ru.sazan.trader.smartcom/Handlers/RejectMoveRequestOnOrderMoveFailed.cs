﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.smartcom.Models;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.smartcom.Handlers
{
    public class RejectMoveRequestOnOrderMoveFailed:AddedItemHandler<OrderMoveFailed>
    {
        private DataContext tradingData;
        private RawTradingDataContext rawData;
        private Logger logger;

        public RejectMoveRequestOnOrderMoveFailed(DataContext tradingData, RawTradingDataContext rawData, Logger logger)
            :base(rawData.GetData<OrderMoveFailed>())
        {
            this.tradingData = tradingData;
            this.rawData = rawData;
            this.logger = logger;
        }

        public override void OnItemAdded(OrderMoveFailed item)
        {
            Order order = FindOrder(item.Cookie);

            if (order == null)
                return;

            OrderMoveRequest request = FindRequest(order);

            if (request == null)
                return;

            if (request.IsFailed)
                return;

            request.Failed(BrokerDateTime.Make(DateTime.Now), item.Reason);

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, получено уведомление об отклонении запроса {2} на сдвиг заявки.", 
                BrokerDateTime.Make(DateTime.Now),
                this.GetType().Name,
                request.ToString()));
        }

        private Order FindOrder(int id)
        {
            try
            {
                return this.tradingData.Get<IEnumerable<Order>>().Single(o => o.Id == id);
            }
            catch
            {
                return null;
            }
        }

        private OrderMoveRequest FindRequest(Order order)
        {
            try
            {
                return this.tradingData.GetMoveRequests(order).Last();
            }
            catch
            {
                return null;
            }
        }
    }
}
