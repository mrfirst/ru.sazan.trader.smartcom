﻿using ru.sazan.trader.Events;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.smartcom.Data
{
    public class ItemAddedLastTimeStamped<T> : TimeStamped
    {
        private ItemHasBeenAddedNotifier<T> notifier;

        public ItemAddedLastTimeStamped(ItemHasBeenAddedNotifier<T> notifier)
        {
            this.notifier = notifier;
            this.notifier.OnItemAdded += UpdateLastNotificationOnItemAdded;
            this.lastNotification = DateTime.MinValue;
        }

        public void UpdateLastNotificationOnItemAdded(T item)
        {
            this.lastNotification = BrokerDateTime.Make(DateTime.Now);
        }

        private DateTime lastNotification;
        public DateTime DateTime
        {
            get { return this.lastNotification; }
        }
    }
}
