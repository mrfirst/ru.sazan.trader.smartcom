﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.smartcom.Models
{
    public class OrderMoveSucceeded:TimeStamped
    {
        public int Cookie { get; set; }
        public string OrderId { get; set; }
        public DateTime DateTime { get; set; }

        public OrderMoveSucceeded(int cookie, string orderId)
        {
            this.Cookie = cookie;
            this.OrderId = orderId;
            this.DateTime = BrokerDateTime.Make(DateTime.Now);
        }
    }
}
