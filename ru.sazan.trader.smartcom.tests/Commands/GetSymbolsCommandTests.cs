﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.smartcom.tests.Mocks;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.smartcom.Commands;

namespace ru.sazan.trader.smartcom.tests.Data
{
    [TestClass]
    public class GetSymbolsCommandTests
    {
        private StServerMockSingleton singleton;
        private StServerClassMock stServer;

        [TestInitialize]
        public void Setup()
        {
            this.singleton = new StServerMockSingleton();
            this.stServer = (StServerClassMock)this.singleton.Instance;
        }

        [TestMethod]
        public void Execute_GetSymbolsCommand()
        {
            Transaction transaction = new GetSymbolsCommand(this.singleton, new NullLogger());
            transaction.Execute();
            Assert.IsTrue(this.stServer.GetSymbolsExecuted);
        }
    }
}
