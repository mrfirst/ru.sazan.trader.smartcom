﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.Models;
using System.Collections.Generic;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Emulation;
using ru.sazan.trader.smartcom.Models;
using SmartCOM3Lib;
using ru.sazan.trader.smartcom.Handlers;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.smartcom.tests.Handlers
{
    [TestClass]
    public class ExpireOrderOnUpdateOrderTests
    {
        private DataContext tradingData;
        private BaseDataContext rawData;

        private Strategy strategy;
        private Signal signal;
        private Order order;

        private ExpireOrderOnUpdateOrder handler;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();
            this.rawData = new RawTradingDataContext();

            this.strategy = new Strategy(1, "Strategy", "ST12345-RF-01", "RTS-9.14", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(this.strategy);

            this.signal = new Signal(this.strategy, DateTime.Now, TradeAction.Buy, OrderType.Stop, 121000, 121000, 0);
            this.order = this.tradingData.AddSignalAndItsOrder(this.signal);

            this.handler =
                new ExpireOrderOnUpdateOrder(this.tradingData, this.rawData, new NullLogger());

            Assert.IsFalse(this.order.IsExpired);
        }

        [TestMethod]
        public void mark_order_as_expired_on_UpdateOrder_with_Expired_state_test()
        {
            UpdateOrder updateOrder = 
                new UpdateOrder(this.strategy.Portfolio,
                    this.strategy.Symbol,
                    StOrder_State.StOrder_State_Expired,
                    StOrder_Action.StOrder_Action_Buy,
                    StOrder_Type.StOrder_Type_Stop,
                    StOrder_Validity.StOrder_Validity_Day,
                    121000,
                    this.strategy.Amount,
                    121000,
                    0,
                    BrokerDateTime.Make(DateTime.Now.AddSeconds(-5)),
                    "1",
                    "2",
                    0,
                    this.order.Id);
            this.rawData.GetData<UpdateOrder>().Add(updateOrder);

            Assert.IsTrue(this.order.IsExpired);
        }

        [TestMethod]
        public void ignore_UpdateOrder_with_Expired_state_and_nonexistent_cookie_test()
        {
            UpdateOrder updateOrder =
                new UpdateOrder(this.strategy.Portfolio,
                    this.strategy.Symbol,
                    StOrder_State.StOrder_State_Expired,
                    StOrder_Action.StOrder_Action_Buy,
                    StOrder_Type.StOrder_Type_Stop,
                    StOrder_Validity.StOrder_Validity_Day,
                    121000,
                    this.strategy.Amount,
                    121000,
                    0,
                    BrokerDateTime.Make(DateTime.Now.AddSeconds(-5)),
                    "1",
                    "2",
                    0,
                    0);
            this.rawData.GetData<UpdateOrder>().Add(updateOrder);

            Assert.IsFalse(this.order.IsExpired);
        }

        [TestMethod]
        public void ignore_UpdateOrder_with_Cancel_state_test()
        {
            UpdateOrder updateOrder =
                new UpdateOrder(this.strategy.Portfolio,
                    this.strategy.Symbol,
                    StOrder_State.StOrder_State_Cancel,
                    StOrder_Action.StOrder_Action_Buy,
                    StOrder_Type.StOrder_Type_Stop,
                    StOrder_Validity.StOrder_Validity_Day,
                    121000,
                    this.strategy.Amount,
                    121000,
                    0,
                    BrokerDateTime.Make(DateTime.Now.AddSeconds(-5)),
                    "1",
                    "2",
                    0,
                    this.order.Id);
            this.rawData.GetData<UpdateOrder>().Add(updateOrder);

            Assert.IsFalse(this.order.IsExpired);
        }

        [TestMethod]
        public void ignore_UpdateOrder_with_ContragentCancel_state_test()
        {
            UpdateOrder updateOrder =
                new UpdateOrder(this.strategy.Portfolio,
                    this.strategy.Symbol,
                    StOrder_State.StOrder_State_ContragentCancel,
                    StOrder_Action.StOrder_Action_Buy,
                    StOrder_Type.StOrder_Type_Stop,
                    StOrder_Validity.StOrder_Validity_Day,
                    121000,
                    this.strategy.Amount,
                    121000,
                    0,
                    BrokerDateTime.Make(DateTime.Now.AddSeconds(-5)),
                    "1",
                    "2",
                    0,
                    this.order.Id);
            this.rawData.GetData<UpdateOrder>().Add(updateOrder);

            Assert.IsFalse(this.order.IsExpired);
        }

        [TestMethod]
        public void ignore_UpdateOrder_with_ContragentReject_state_test()
        {
            UpdateOrder updateOrder =
                new UpdateOrder(this.strategy.Portfolio,
                    this.strategy.Symbol,
                    StOrder_State.StOrder_State_ContragentReject,
                    StOrder_Action.StOrder_Action_Buy,
                    StOrder_Type.StOrder_Type_Stop,
                    StOrder_Validity.StOrder_Validity_Day,
                    121000,
                    this.strategy.Amount,
                    121000,
                    0,
                    BrokerDateTime.Make(DateTime.Now.AddSeconds(-5)),
                    "1",
                    "2",
                    0,
                    this.order.Id);
            this.rawData.GetData<UpdateOrder>().Add(updateOrder);

            Assert.IsFalse(this.order.IsExpired);
        }

        [TestMethod]
        public void ignore_UpdateOrder_with_Filled_state_test()
        {
            UpdateOrder updateOrder =
                new UpdateOrder(this.strategy.Portfolio,
                    this.strategy.Symbol,
                    StOrder_State.StOrder_State_Filled,
                    StOrder_Action.StOrder_Action_Buy,
                    StOrder_Type.StOrder_Type_Stop,
                    StOrder_Validity.StOrder_Validity_Day,
                    121000,
                    this.strategy.Amount,
                    121000,
                    0,
                    BrokerDateTime.Make(DateTime.Now.AddSeconds(-5)),
                    "1",
                    "2",
                    0,
                    this.order.Id);
            this.rawData.GetData<UpdateOrder>().Add(updateOrder);

            Assert.IsFalse(this.order.IsExpired);
        }

        [TestMethod]
        public void ignore_UpdateOrder_with_Open_state_test()
        {
            UpdateOrder updateOrder =
                new UpdateOrder(this.strategy.Portfolio,
                    this.strategy.Symbol,
                    StOrder_State.StOrder_State_Open,
                    StOrder_Action.StOrder_Action_Buy,
                    StOrder_Type.StOrder_Type_Stop,
                    StOrder_Validity.StOrder_Validity_Day,
                    121000,
                    this.strategy.Amount,
                    121000,
                    0,
                    BrokerDateTime.Make(DateTime.Now.AddSeconds(-5)),
                    "1",
                    "2",
                    0,
                    this.order.Id);
            this.rawData.GetData<UpdateOrder>().Add(updateOrder);

            Assert.IsFalse(this.order.IsExpired);
        }

        [TestMethod]
        public void ignore_UpdateOrder_with_Partial_state_test()
        {
            UpdateOrder updateOrder =
                new UpdateOrder(this.strategy.Portfolio,
                    this.strategy.Symbol,
                    StOrder_State.StOrder_State_Partial,
                    StOrder_Action.StOrder_Action_Buy,
                    StOrder_Type.StOrder_Type_Stop,
                    StOrder_Validity.StOrder_Validity_Day,
                    121000,
                    this.strategy.Amount,
                    121000,
                    0,
                    BrokerDateTime.Make(DateTime.Now.AddSeconds(-5)),
                    "1",
                    "2",
                    0,
                    this.order.Id);
            this.rawData.GetData<UpdateOrder>().Add(updateOrder);

            Assert.IsFalse(this.order.IsExpired);
        }

        [TestMethod]
        public void ignore_UpdateOrder_with_Pending_state_test()
        {
            UpdateOrder updateOrder =
                new UpdateOrder(this.strategy.Portfolio,
                    this.strategy.Symbol,
                    StOrder_State.StOrder_State_Pending,
                    StOrder_Action.StOrder_Action_Buy,
                    StOrder_Type.StOrder_Type_Stop,
                    StOrder_Validity.StOrder_Validity_Day,
                    121000,
                    this.strategy.Amount,
                    121000,
                    0,
                    BrokerDateTime.Make(DateTime.Now.AddSeconds(-5)),
                    "1",
                    "2",
                    0,
                    this.order.Id);
            this.rawData.GetData<UpdateOrder>().Add(updateOrder);

            Assert.IsFalse(this.order.IsExpired);
        }

        [TestMethod]
        public void ignore_UpdateOrder_with_Submited_state_test()
        {
            UpdateOrder updateOrder =
                new UpdateOrder(this.strategy.Portfolio,
                    this.strategy.Symbol,
                    StOrder_State.StOrder_State_Submited,
                    StOrder_Action.StOrder_Action_Buy,
                    StOrder_Type.StOrder_Type_Stop,
                    StOrder_Validity.StOrder_Validity_Day,
                    121000,
                    this.strategy.Amount,
                    121000,
                    0,
                    BrokerDateTime.Make(DateTime.Now.AddSeconds(-5)),
                    "1",
                    "2",
                    0,
                    this.order.Id);
            this.rawData.GetData<UpdateOrder>().Add(updateOrder);

            Assert.IsFalse(this.order.IsExpired);
        }

        [TestMethod]
        public void ignore_UpdateOrder_with_SystemCancel_state_test()
        {
            UpdateOrder updateOrder =
                new UpdateOrder(this.strategy.Portfolio,
                    this.strategy.Symbol,
                    StOrder_State.StOrder_State_SystemCancel,
                    StOrder_Action.StOrder_Action_Buy,
                    StOrder_Type.StOrder_Type_Stop,
                    StOrder_Validity.StOrder_Validity_Day,
                    121000,
                    this.strategy.Amount,
                    121000,
                    0,
                    BrokerDateTime.Make(DateTime.Now.AddSeconds(-5)),
                    "1",
                    "2",
                    0,
                    this.order.Id);
            this.rawData.GetData<UpdateOrder>().Add(updateOrder);

            Assert.IsFalse(this.order.IsExpired);
        }

        [TestMethod]
        public void ignore_UpdateOrder_with_SystemReject_state_test()
        {
            UpdateOrder updateOrder =
                new UpdateOrder(this.strategy.Portfolio,
                    this.strategy.Symbol,
                    StOrder_State.StOrder_State_SystemReject,
                    StOrder_Action.StOrder_Action_Buy,
                    StOrder_Type.StOrder_Type_Stop,
                    StOrder_Validity.StOrder_Validity_Day,
                    121000,
                    this.strategy.Amount,
                    121000,
                    0,
                    BrokerDateTime.Make(DateTime.Now.AddSeconds(-5)),
                    "1",
                    "2",
                    0,
                    this.order.Id);
            this.rawData.GetData<UpdateOrder>().Add(updateOrder);

            Assert.IsFalse(this.order.IsExpired);
        }
    }
}
