﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Events;
using ru.sazan.trader.Data;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Utility;
using ru.sazan.trader.smartcom.Events;
using ru.sazan.trader.smartcom.Data;

namespace ru.sazan.trader.smartcom.tests.Data
{
    [TestClass]
    public class ItemAddedLastTimeStampedTests
    {
        private DataContext dataContext;
        private TimeStamped lastItemAdded;

        [TestInitialize]
        public void Setup()
        {
            this.dataContext = new TradingDataContext();
            this.lastItemAdded =
                new ItemAddedLastTimeStamped<Tick>(this.dataContext.Get<ObservableCollection<Tick>>());

            Assert.IsTrue(this.lastItemAdded.DateTime == DateTime.MinValue);
        }

        [TestMethod]
        public void notify_DateTime_of_last_incoming_tick_test()
        {
            this.dataContext.Get<ObservableCollection<Tick>>().Add(new Tick("RTS-9.14", DateTime.Now, TradeAction.Buy, 125000, 3));
            Assert.AreNotEqual(DateTime.MinValue, this.lastItemAdded.DateTime);
        }
    }
}
