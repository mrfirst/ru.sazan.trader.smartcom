﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.smartcom.tests.Data
{
    [TestClass]
    public class TickDataFeedUpdateTimeStampTests
    {        
        [TestMethod]
        public void TickDataFeedUpdateTimeStamp_is_singleton_test()
        {
            ItemAddedLastTimeStamped<Tick> first = TickDataFeedUpdateTimeStamp.Instance;
            ItemAddedLastTimeStamped<Tick> second = TickDataFeedUpdateTimeStamp.Instance;
            
            Assert.AreSame(first, second);
            Assert.AreEqual(DateTime.MinValue, first.DateTime);
            Assert.AreEqual(DateTime.MinValue, second.DateTime);
        }
    }
}
