﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.smartcom.Data;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.smartcom.tests.Data
{
    [TestClass]
    public class RawTradingDataTests
    {
        [TestMethod]
        public void RawTradingData_Is_Singleton()
        {
            RawTradingData d = RawTradingData.Instance;
            RawTradingData d2 = RawTradingData.Instance;

            Assert.AreSame(d, d2);
        }

        [TestMethod]
        public void RawTradingData_Inherits_RawTradingDataContext()
        {
            Assert.IsInstanceOfType(RawTradingData.Instance, typeof(RawTradingDataContext));
        }

        [TestMethod]
        public void MarketData_Is_DataContext()
        {
            Assert.IsTrue(RawTradingData.Instance is BaseDataContext);
        }
    }
}
