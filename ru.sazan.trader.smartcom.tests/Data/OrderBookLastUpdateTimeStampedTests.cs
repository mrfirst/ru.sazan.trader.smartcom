﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Events;
using ru.sazan.trader.Data;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Utility;
using ru.sazan.trader.smartcom.Events;
using ru.sazan.trader.smartcom.Data;

namespace ru.sazan.trader.smartcom.tests.Data
{
    [TestClass]
    public class OrderBookLastUpdateTimeStampedTests
    {
        private QuoteProvider orderBook;
        private TimeStamped lastOrderBookUpdate;

        [TestInitialize]
        public void Setup()
        {
            this.orderBook = new OrderBookContext();

            this.lastOrderBookUpdate =
                new OrderBookLastUpdateTimeStamped(this.orderBook);

            Assert.IsTrue(this.lastOrderBookUpdate.DateTime == DateTime.MinValue);
        }

        [TestMethod]
        public void notify_DateTime_of_last_OrderBook_update_test()
        {
            this.orderBook.Update(1, "RTS-9.14", 135000, 32, 135010, 11);
            Assert.AreNotEqual(DateTime.MinValue, this.lastOrderBookUpdate.DateTime);
        }
    }
}
