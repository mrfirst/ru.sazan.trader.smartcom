﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCOM3Lib;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.smartcom.tests.Mocks
{
    public class StServerMockSingleton:GenericSingleton<StServer>
    {
        public StServer Instance
        {
            get { return MockSmartCom.Instance; }
        }

        public void Destroy()
        {
            MockSmartCom.Destroy();
        }

        public bool IsNull
        {
            get { return MockSmartCom.IsNull; }
        }
    }
}
