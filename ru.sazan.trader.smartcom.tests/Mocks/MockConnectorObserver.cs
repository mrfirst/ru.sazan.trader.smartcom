﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Events;

namespace ru.sazan.trader.smartcom.tests.Mocks
{
    public class MockConnectorObserver:Observer
    {
        public bool DisconnectDetected { get; set; }

        public void Update()
        {
            this.DisconnectDetected = true;
        }
    }
}
